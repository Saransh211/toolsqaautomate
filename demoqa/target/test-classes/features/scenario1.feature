#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
Feature: Test Login page of ToolsQA
		
	Background: Browser should be triggered
		When User trigger the browser and enter URL
		Then Website will be displayed

@Tag1
	Scenario: Check whether Book Store Application link is working
		When User clicks on the Book Store Application link
		Then User should land of the Book Store Application page

@Tag2
  Scenario: Login with correct credentials
    Given User enter correct credentials
    When Press Login button
    Then User should be logged in the system
