package Utility;

import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import PageObjects.initBrowser;
public class WaitClass 
{
	//this function will take the locator(xpath or css path) as a argument and wait till that particular web element is not clickble and once it is clickable it will click on it using java script   
	//@SuppressWarnings("unchecked")
//	public static void waitANDclick(WebElement locator)
//	{
//		new WebDriverWait(initBrowser.driver, Duration.ofSeconds(60))
//		.pollingEvery(Duration.ofMillis(50))
//		.ignoring(Exception.class)
//		.until(ExpectedConditions.elementToBeClickable(locator));
//		JavascriptExecutor js =  (JavascriptExecutor)initBrowser.driver;
//		js.executeScript("argument[0].click()",locator);
//	}
//	Wait<WebDriver> wait = new FluentWait<WebDriver>(initBrowser.driver)							
//			.withTimeout(30, TimeUnit.SECONDS) 			
//			.pollingEvery(5, TimeUnit.SECONDS) 			
//			.ignoring(NoSuchElementException.class);
	//this function is similar to the one at the top the only difference is this one will only wait and won't click
	//@SuppressWarnings("unchecked")
//	public static void wait(WebDriver, WebElement locator)
//	{
//		new WebDriverWait(initBrowser.driver, Duration.ofSeconds(60))
//		.pollingEvery(Duration.ofMillis(60))
//		.ignoring(Exception.class)
//		.until(ExpectedConditions.visibilityOfElementLocated((By) locator));
//		WebElement clickseleniumlink = wait.until(new Function<WebDriver, WebElement>(){
//			
//			public WebElement apply(WebDriver driver ) {
//				return driver.findElement(By.xpath("/html/body/div[1]/section/div[2]/div/div[1]/div/div[1]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div/a/i"));
//			}
//		}
		
	}

