package Utility;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src\\test\\resources\\features\\scenario1.feature", glue = {"stepDefinition"}, tags="@Tag1")
public class testRunner {}


//Run with Junit
//public class testRunner 
//{
//	WebDriver driver;
//	@Before
//	public void triggerBrowser()
//	{
//		System.setProperty("WebDriver.chrome.driver", "C:\\drivers\\chromedriver_87.exe");
//		driver = new ChromeDriver();
//		driver.get("https://demoqa.com/");	
//		driver.manage().window().maximize();
//	}
//	@Test
//	public void testMethod()
//	{
//		System.out.println("Test function");
//	}
//}