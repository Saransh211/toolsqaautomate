package stepDefinition;

import java.io.IOException;

import org.openqa.selenium.support.PageFactory;
import PageObjects.demoQAHomePage;
import PageObjects.initBrowser;
import Utility.WebElementsRepo;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import junit.framework.Assert;

public class stepDefinition 
{	
	//Background stuff
	@When("User trigger the browser and enter URL")
	public void user_trigger_the_browser_and_enter_url() 
	{
	//	System.out.println("@When called");
		initBrowser.triggerBrowser();
	}

	@Then("Website will be displayed")
	public void website_will_be_displayed() 
	{
		initBrowser.openURL();
		PageFactory.initElements(initBrowser.driver, WebElementsRepo.class);	
	}
	//-----------------------

	//Scenario: Check whether Book Store Application link is working

	@When("User clicks on the Book Store Application link")
	public void user_clicks_on_the_book_store_application_link() throws InterruptedException 
	{
		System.out.println("A1");
		demoQAHomePage.OpenBookStoreApplication();
	}

	@SuppressWarnings("deprecation")
	@Then("User should land of the Book Store Application page")
	public void user_should_land_of_the_book_store_application_page() throws IOException 
	{
		Assert.assertEquals("https://demoqa.com/books", initBrowser.driver.getCurrentUrl());
		 Runtime.getRuntime().exec("taskkill /F /IM ChromeDriver.exe") ;
	}		
	//------------------------
	@Given("User enter correct credentials")
	public void user_enter_correct_credentials() 
	{
		//WebElementsRepo.OpenBookStoreApplication();
	}

	@When("Press Login button")
	public void press_Login_button() 
	{

	}

	@Then("User should be logged in the system")
	public void user_should_be_logged_in_the_system() 
	{

	}
}
